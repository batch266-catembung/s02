package com.zuitt;


import java.util.ArrayList;
import java.util.HashMap;

public class activity {
    public static void main(String[] args) {

        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;
        System.out.println("the first prime number is: "+primeNumbers[0]);

        //ArrayList<String> people = new ArrayList<String>(Arrays.asList());
        ArrayList<String> people = new ArrayList<>();
        people.add("John");
        people.add("Jane");
        people.add("Chloe");
        people.add("Zoey");
        System.out.println("my friends are: "+people);

        //HashMap<String, Integer> items = new HashMap<String, Integer>();
        HashMap<String, Integer> items = new HashMap<>();
        items.put("toothpaste", 15);
        items.put("toothbrush", 20);
        items.put("soap", 12);
        System.out.println("our current inventory consist of: "+items);
    }
}
