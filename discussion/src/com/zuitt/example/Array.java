package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {
        //[SECTION] Java Collection
        //are single unit of objects
//        data with relevant and connected values
//
//In java, arrays are container of values of the same type given a predefined amount of values.
        // java arrays are more rigid, once the size and data type are defined, they can no longer be changed.

//        syntax: array
        /*
        dataType[] identifier = new dataType[numOfElements];
       */

        int[] intArray = new int[5]; //set the size of an array
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
        // will result error -> out of bounds
        //intArray[5] = 99;
        System.out.println(intArray); // this will return memory address
        System.out.println(Arrays.toString(intArray)); // show the array

//        string array
//        syntax : array declaration with initialization
//        dataType identifier = {elementA, elementB, elementC};
        String[] names = {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));

//       sample java array method
//        SORT
        Arrays.sort(intArray);
        System.out.println("order of items after sort method " + Arrays.toString(intArray));
        System.out.println(intArray[0]);

        String[][] classroom = new String[3][3];
        //        first row
        classroom[0][0]= "Athos";
        classroom[0][1]= "Porthos";
        classroom[0][2]= "Aramis";
        //        second row
        classroom[1][0]= "Brandon";
        classroom[1][1]= "Junjun";
        classroom[1][2]= "Jobert";
        //        third row
        classroom[2][0]= "Mickey";
        classroom[2][1]= "Donald";
        classroom[2][2]= "Goofy";

        System.out.println(Arrays.toString(classroom));//
        System.out.println(Arrays.deepToString(classroom));//

//       [SECTION] array list
//        are resizezable arrays, where elements can be added or removed
//        syntax: ArrayList
//        ArrayList<T> identifier = new Arraylist<T>();
//
//        Declare ArrayList
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

//      add elements
        students.add("John");
        students.add("Paul");
        students.add("Jake");
        System.out.println(students);

//      getting an element
        System.out.println(students.get(0));
        System.out.println(students.get(1));

//      adding an element in a specific index
        students.add(0,"Joey");
        System.out.println(students);
//        students.add(2,"Robert");
//        System.out.println(students);

//      update elements in specific index
        students.set(0, "George");
        System.out.println(students);

//        remove specific index
        students.remove(1);
        System.out.println(students);
        System.out.println(students.size());

//      remove all elements
        students.clear();
        System.out.println(students);

//        get arraylist size
        System.out.println(students.size());

//        [SECTION] Hashmaps
//        syntax: hashmap
//        HashMap<T, T> identifier = new HashMap<T, T>();

        HashMap<String, Integer> job_postion = new HashMap<String, Integer>();
//        adding element ot hashmap
        job_postion.put("Brandon", 5);
        job_postion.put("Alice", 2);
        System.out.println(job_postion);
        System.out.println(job_postion.get("Brandon"));
        System.out.println(job_postion.get("brandon"));
        System.out.println(job_postion.values());
        job_postion.clear();
        System.out.println(job_postion);








    }
}

