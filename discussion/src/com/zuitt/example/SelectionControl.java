package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {
        //[SECTION] Java Operators
        // Arithmetic - +, - , *, /, %
        // Comparison - >, <, >=, <=, ==, !=
        // Logical -> &&, ||, !
        // Assignment -> =

//        [SECTION] Control structures (if else statements)
        int num = 36;
        if (num % 5 == 0){
            System.out.println(num + " is divisible by 5");
        }else {
            System.out.println(num + " is NOT divisible by 5");
        }

//        ternary
        int grade = 90;

        Boolean result = (grade >= 75) ? true : false;
        System.out.println(result);

//      switch cases
        Scanner numberScanner = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int directionValue = numberScanner.nextInt();
        switch (directionValue){
//            a case block within a switch statement, represent a single case.
            case 1:
                System.out.println("north");
                break;
            case 2:
                System.out.println("east");
            case 3:
                System.out.println("west");
                break;
            case 4:
                System.out.println("south");
                break;
            default:
                System.out.println("invalid input");
                break;
        }



    }
}
